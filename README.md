# Dark Saber Web Development Course

This class was built to support airmen of the United States Air Force to gain familiarity with modern web dev technologies

## Core Topics

- JS
- ReactJS
- NodeJS
- ExpressJS
- Docker
- Git / GitLab
- AWS
- MongoDB / DynamoDB
- Bash

## Before You Start

Prior to starting the class, we *highly* recommend that you complete the following pre-requisites

1. JavaScript Fundementals: https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/
2. Front End Development (ReactJS, Redux sections): https://www.freecodecamp.org/learn/front-end-development-libraries/#react

Once you complete these, you can start on Lab 1 of the course
