# Lab 5 - Day of the Week Generator with React

In this lab, we will put together what you've learned so far in order
to make a program that generate the day of the week for any given date (AD calendar)
and create a `ReactJS` application to make it interactive!

Overall, we will learn the following concepts:

1. Bringing in `JavaScript` files to a `ReactJS` project
2. Adding form fields to a `ReactJS` page
3. State management
4. Adding interactive elements using `Material UI (MUI)`
5. Adding a `MUI` `Table` to keep a history of transactions


Your program will accept a month, day, and year and must produce the day of the week.

For example
```
What is the month (1 to 12)? 1
What is the day (1 to 31)? 30
What is the year (e.g., 2007) 2007
Your date falls on a Tuesday
```

In addition, you must take into account leap years. The rules of leap years
were developed by the Romans where the following conditions need to be met:

1. Every 4 years is a leap year
2. Every 100 years a leap year is skipped unless the year is divisible by 400

**Note**: You **CANNOT** use any `JavaScript` `datetime` libraries to develop your solution.

#### Solution Requirements

Your solution must be broken into multiple functions to help you solve the
solution. Do not write one big mega-function (that would be mega-not-cool).

#### Suggestions

- Try to use `[].includes()` in your solution, this will be helpful for parsing input.
- Try to use a `switch` statement in your solution
- Break out the problem into functions
- **Suggested functions**

  1. A function that determines if a given year is a leap year
  2. A function that computes the number of days that have past
  3. A function that takes the number of days that have past and solves the day of the week challenge


### Part 0: Start with the Base

To begin, work from the `lab5-base` project in this directory.

You'll need to start by installing the packages within `package.json` which contains all of the current project dependencies. (You did not have to do this in `lab 4` because `create-react-app` did it for you)

To install the packages, simply run `npm install` from the root directory of the project `lab5-base/`

For this lab, we will use `Material UI (MUI)` which you'll need to also install. `MUI` is a library of user interface (UI) components standardized into a simple, easy to use framework. See [Getting Started](https://mui.com/material-ui/getting-started/installation/) for instructions and install with `npm`

Once you perform the installation of the required libraries, you can now run the app using `npm start`

Signature `________________________________`

### Part 1: Collecting and Displaying User Input

In this section, we will collect user input using input components from `MUI`.

Our objective with Material UI is to place three [Text Field](https://mui.com/material-ui/react-text-field/) components on the screen. One to capture each of the following inputs:

1. Day
2. Month
3. Year

As the user provides input to a [Text Field](https://mui.com/material-ui/react-text-field/), display the value as static text elsewhere on the page. (e.g. If the user types in `3` for the `Month` input, display a block with the message `Month: 3` or similar)

Make use of ReactJS state managmenet! You should use the `useState()` hook for this task, defining state variables for the `day`, `month`, and `year`

As an extra challenge, center these elements on the screen using a [Box](https://mui.com/material-ui/react-box/) component

Signature `________________________________`

### Part 2: Validating User Input

Ensure user input to ensure the day, month, and year inputs are valid

```
Day:
    Min: 1
    Max: [28-31] - variable depending on input month & year
Month:
    Min: 1
    Max: 12
Year:
    Min: 0
    Max: 10000 (or greater)
```

When input is `invalid`, use [Text Field Validation](https://mui.com/material-ui/react-text-field/#validation) to highlight the component red.

Signature `________________________________`

### Part 3: Day of the Week Calculation

When a user has provided a valid day, month, and year, enable a [Button](https://mui.com/material-ui/react-button/) for the user to click in order to calculate the day of the week. Once calculated, display the result

#### Example

##### Input

- Day: 1
- Month: 3
- Year: 2007

##### Output

- Day of the Week: Wednesday

To accomplish this, you can use your code from `lab2`

### Formatting

**The final webpage parts**

1. A title showing the title of the application and a sub-title for the author's name
2. A label that displays the computed day of the week
3. Three `Text Field` components for each input (day, month, year)
4. A `Button` that, when clicked, calculates the day of the week. **Note**: This button should be disabled when the input fields contain invalid input.
5. A `Button` that clears the input after a day of the week calculation is performed. **Note**: This button should be disabled when there is no day of the week to clear

Signature `________________________________`

### Part 4: Create a Log Table

For this section of this lab, create a [Table](https://mui.com/material-ui/react-table/) component where a log of calculated days of the week are shown. The entered `day`, `month`, and `year` in addition to the computed `day of the week` value should be displayed

After a user calculates the day of the week, add it to this table and clear the input text fields

The layout of the table should look like this

| Day | Month | Year | Day of Week |
| --- | ----- | ---- | ----------- |
| 1   | 1     | 2007 | Monday      |
| 18  | 4     | 2023 | Tuesday     |

##### Hint: The `useState()` hook in `ReactJS` can manage `Arrays` and `Objects`

##### Bonus: Create a button that clears the table either in the table or under it

Signature `________________________________`

### Part 5: Styling the App

For the final part of the lab, we are going to clean up our project and make it look clean

To do this, we will abide by the following layout where on the left side of the
screen, we will show the input fields and on the right side of the screen, we
will show the table

The elements text fields and labels on the left should span `80%` of the screen
width wise

The table should span `100%` the entire width and height of the right side of the
parent and each of the labels on the left-hand side should also span `100%` of the
width. In addition, the table should have a minWidth property set of `400`

| Input Field Contents              | Table Contents |
| --------------------------------- | -------------- |
| Day of the Week Generator - Lab 5 |                |
| Day of the Week: <DOW>            |                |
| Input Field: Day                  |                |
| Input Field: Month                |                |
| Input Field: Year                 |                |
| Button Calculate                  |                |

To accomplish this, we suggest you use `MUI` `Stack`, `Container` or `Grid`

To begin, you'll want to create a `Stack` with 2 `Containers` within it

Each container will manage either the left or right side of the page

Look up how to pass properties to a `Stack` in order to ensure that it spans `80%` of the width

Ensure the contents of each container span the full width of each container

How you layout your components is up to you. Add some colors and look up `MUI`
documentation to figure out how

Be creative here :)

Signature `________________________________`

### Part 6: Pushing to Gitlab

Once you completed all parts of the lab, and you have a verified working solution, you're ready to commit your progress to Git just like you did in previous labs.

Let us know when you've pushed your code to GitLab.

Signature `________________________________`

#### Congrats! You've competed Lab 5!
