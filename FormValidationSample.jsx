import {useState} from "react";
import {Box, TextField} from "@mui/material";

export default function ValidationTextFields() {
  const [inError, setInError] = useState(false);
  const [input, setInput] = useState("");

  const validateInput = (val) => {
    setInput(val); // Set the Input
    if (val === "" || val === undefined || (val > 0 && val <= 10)) {
      setInError(false);
    } else {
      setInError(true);
    }
  };

  return (
    <Box
    >
      <div>
        <TextField
          error={inError}
          label="Demo Input Validation"
          value={input}
          onChange={(e) => validateInput(e.target.value)}
          helperText={
            inError ? "Incorrect entry, must be number between 1-10" : " "
          }
        />
      </div>
    </Box>
  );
}
