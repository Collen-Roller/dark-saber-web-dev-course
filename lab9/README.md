# Lab 9 - Full Stack Application

In this lab, we will finally merge our client TODO List application from `lab7` together with our API from `lab8`

## Part 0: Start with your Lab 7 and Lab 8 Solutions

In this part, you'll be tasked to create a new project folder within `day4/lab9/<YOUR_APP_NAME>`

Within this folder, you'll create a folder called `client` and another folder called `api`

Next, you'll be tasked to copy all of your code from `lab7` into the `client` folder and the code from `lab8` into the `api` folder

For example, you'll have the following

`my_lab9_solution/client...`

`my_lab9_solution/api...`

Once you have this, you'll be running 2 programs from your project.

Open one terminal session to run your `ReactJS` application from the `client` directory which will run on `localhost:3000` another another terminal session to run your `API` application from the `backend` application which will run on `localhost:4000`

You should now have your `lab7` and `lab8` solutions running on different ports

## Part 1: Connecting the Systems

In this part of the application, you'll be tasked to replace `JSON server` with your `API`. You can do this by modifying your `Axios` endpoint to connect to your `API`. Note, you might need to modify the endpoint for this depending on what port you hosted your `JSON Server` on

Next, once you get it integrated, ensure you test all of your functions to ensure they are working properly

To complete this, testing multiple use-cases for processing `GET` `POST` `PATCH` and `DELETE` requests.

We are not defining these test cases for you. Ensure that the data you create is updating within the `API`, similarity, ensure the data is posting, updating and deleting properly. Document your test-cases within a TEST.md `Markdown` file where you will document your tests

###### Suggestion: We suggest you mirror your tests that you define in `Postman`

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

## Part 2: Redux Tool Kit

This core part of this lab is to integrate Redux Tool Kit (`RTK`) into your application. `RTK` gives you the ability to manage your state without setting massive amounts of local state variables or global state variables that you pass down to your components via prop drilling. Below is a link to `RTK` so you can get started

##### Note: You'll want use the `JavaScript` code samples, not the `TypeScript` samples.

https://redux-toolkit.js.org/tutorials/quick-start

Your task is to integrate `RTK` into your application by using it to manage the state of your `TODOList` application. In `RTK` you are tasked to define a redux store that manages your queries to the Mongo and maintains your state.

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

## Part 3: Pushing to Git

Once you completed all parts of the lab and you have a verified working solution,
you're ready to commit your progress to GitLab using `Git CLI`!

Ensure you're using your project branch

`git add .`

`git commit -m "Adding files from lab 9`

`git push`

Let us know when you've pushed your code to GitLab

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

#### Congrats - You've made your first full stack application and completed Lab 9!