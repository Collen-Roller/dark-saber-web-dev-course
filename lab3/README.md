# Lab 3 - TODO

### Submission

When you believe you have the answer, call an instructor over to review your work

Signature `________________________________`

### Part 1: Pushing to Gitlab

Once you completed all parts of the lab, and you have a verified working solution, you're ready to commit your progress to Git!

Ensure you're using your project branch:

```shell
$ git branch
  master
* my-cool-branch
```

Commit your code:

```shell
$ git add .
$ git commit -m "Adding files from this lab"
$ git push

# alternatively
$ git commit -a -m "<my commit message>"
$ git push
```

Let us know when you've pushed your code to GitLab

Signature `________________________________`

### Congrats! You've completed Lab 3!
