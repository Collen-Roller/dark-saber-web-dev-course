# Lab 4 - Create React App

#### Getting Stated with React

It's time to write your first React application, first you'll create a React application with `create-react-app`

You should have this installed already, if not, install the package using `npm` (node package manager)

### Part 1: Build a React application

Name your new react app `lab4-<YOUR_LAST_NAME>` (e.g. "lab4-roller")

```bash
npx create-react-app lab4-<YOUR_LAST_NAME>
cd lab4-<YOUR_LAST_NAME>
npm start
```

Once you do this, you'll have a local application running on port `3000`. To view your application, go to http://localhost:3000 in a web browser

### Part 2: Project Layout

As noted in the lecture, you saw a `package.json` file. This file allows you to manage the application dependencies, run-time scripts, and other metadata about your application.

Within the `lab4-<YOUR_LAST_NAME>/public` folder, you'll see the core `index.html` file. This file contains the basic framework for React to inject your React application. The way ReactJS works is by manipulating the Document Object Model (DOM). The React application attaches to the `<div id="root"></div>` element in `index.html` and injects/modifies content from the `#root` div. This makes it so you'll never have to manually write `html`, rather, you'll just write ReactJS components.

Let's modify `index.html` a bit. Change the title of your webpage by modifying the text within the `<title></title>` element to something different. You should see it change in the browser once you save the changes as React will "hot-reload" the page.

Next, go to the `manifest.json` directory and modify both the name and the short name of your project to something different.

Next, let's change the site's favicon. You'll should see `favicon.ico` in the `public` folder. Head to https://realfavicongenerator.net/ to create a custom icon and replace the default image in the folder. You'll see it popup at the top of your browser.

**Note: You might need to clear your browser cache to see it (hard refresh shortcut: `CTRL + F5`)**

### Part 3: Modifying the React App

Next, we'll modify the React App itself. Like most programs, ReactJS applications have a `parent-level` program that kicks them off. Back within the `index.html` file, you'll see a `div` with an `id=root`. If you look in the `src` directory, you'll see `src/index.js` which references the `root` div element within the DOM. This is how ReactJS injects all the components and children components to the html base.

```jsx
// Core code to run a ReactJS app
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App /> {/* App is the highest level component */}
  </React.StrictMode>
);
```

Go to `src/App.js` where you'll see the primary ReactJS code. This is where you'll see your first `JSX` segment, which is a syntax extension to JavaScript. ReactJS is one framework that allows you to use `JSX`. Within each ReactJS component, you must return a `JSX` element which is what will be rendered within the DOM.

For this step, create a **header** on the page the contains the following

1. Your Name
2. Class / Lab
3. Current Date
4. Something About You

**Hint**: JSX supports all native HTML elements.

Next, you'll see the ReactJS logo spinning on the web-page.

Replace the image with your own image. You can use an online SVG converter such as https://image.online-convert.com/convert-to-svg

**Hint**: You'll need to import the new file and use it in the JSX

Signature `________________________________`

##### 4: Pushing to Gitlab

Once you completed all parts of the lab, and you have a verified working solution, you're ready to commit your progress to Git!

Ensure you're using your project branch:

```bash
git branch
  master
* my-cool-branch
```

Commit your code:

```bash
git add .
git commit -m "Adding files from this lab"
git push

# alternatively
git commit -a -m "<my commit message>"
git push
```

Let us know when you've pushed your code to GitLab

Signature `________________________________`

### Congrats! You've completed Lab 4!
