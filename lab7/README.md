# Lab 7 - "TODO List" Application

In lab 7, we will build a `TODO list` application!

Overall, we will learn / re-enforce the following concepts:

1. `MUI` components - `Table` `Modal` `Dialog`
2. Managing Local State
3. Using JSON Server
4. Creating / Using a Reducer

## Part 0: Start with the Base

To begin, `cd` into the `lab7-base` project

Install existing dependencies with `npm install`

You are now able to run the app using `npm start`

Note, we've already added MaterialUI as a dependency, the core library you'll need to for this lab. Throughout the lab, we may instruct you to install additional dependencies.

## Part 1: TODO List Application

Let's build the core `TODO List` section of the application

In this lab, we will build a single page application (SPA). Eventually, we will add a `Modal` component, a database, and more!

First, create the folder `src/components` to hold all components for your application. Next, create `TODOList.jsx` to house your main component.

Once created, update your `index.js` and replace the existing `App` component with your new `TODOList` component.

Next, create a `MUI` [Table](https://mui.com/material-ui/react-table/) within the `TODOList.jsx` component. This will be the primary `TODO List` for the application.

This `Table` will display all of the `TODO list` data. Specifically, the `Table` will show the following pieces of information for each item added:

1. Item Number
2. Priority
3. Item Name
4. Item Details
5. Due Date
6. Status
7. Created At
8. Updated At

Signature `________________________________`

## Part 2: MUI Theme

Now let's add an application theme to the core `TODO List` section. So far, we've been working primarily using `App.css` or defining `in-line` styles for `MUI` components. For this part, we will define a `MUI Theme` to configure the following:

1. Background Color
2. Primary Color
3. Secondary Color

A theme provides the ability to quickly configure a top-level component and all the children components.

There are multiple ways to apply a `MUI` theme

We suggest creating a new file called `Theme.jsx` where you'll define and export your theme. From there, you can import your theme into the application and use it. Look up how to apply a `MUI` theme in the documentation page: https://mui.com/material-ui/customization/theming/

Specifically, let's create a `ThemeProvider` that provides the theme to the application. You should do this in your `index.js` file (**hint**: The `ThemeProvider` will act as a component and should wrap your existing `TODOList` component)

Be creative with your theme and colors. Once you have the web-page and `Table` looking nice and laid-out properly, call us over to show us your progress.

###### Note: Keep in mind, this part is important as we'll be using the `TODO list` application that you create in this lab for the remainder of the course.

Signature `________________________________`

## Part 3: Seed Data

Now we will add data to our `Table`.

In future parts, we will add the ability to import records using a `database` and an `API`. For now, will store and retrieve sample data using an `npm` library called `JSON Server`.

### JSON Server

`JSON` stands for `JavaScript Object Notation` and is a widely used format across the software industry. Most frontend developers work separately from backend developers but in parallel. During the planning stage, a data format is agreed upon between each group in terms of how the data shall be represented/formatted (i.e. - JSON, XML). Once this happens, the frontend team normally generates seed data (sample data) to build the front-facing aspects of the application while the backend team works on building the `data layer` and an `API` to communicate with the `database`.

One tool that is commonly used by frontend developers to manipulate seed data is `JSON Server`. For our project, we will be using `JSON Server` to provide sample records for our `TODO List` application. `JSON Server` will automatically generate a `db.json` file with some data when you first start the service. See example from their [Getting Started](https://www.npmjs.com/package/json-server#getting-started) page:

**DO NOT CREATE `db.json` FILE**

```json
{
  "posts": [{ "id": 1, "title": "json-server", "author": "typicode" }],
  "comments": [{ "id": 1, "body": "some comment", "postId": 1 }],
  "profile": { "name": "typicode" }
}
```

In total, create `15` TODO record examples in the `db.json`.

For each entry, you should expect the following properties:

1. id (auto generated when entries are created)
2. Priority (String - High, Medium, Low)
3. Item Name - (String)
4. Item Details (String)
5. Due Date (Date)
6. Status (String - Completed, Open)
7. Created At (Date)
8. Updated At (Date)

### State management

**Note** - It is not necessary to import or otherwise bring in your `15` examples in this step. We're merely building infrastructure for later use.

Let's use the `useState` hook to house our `JSON` data. For now, let's initialize the state with a couple of our rows for testing.

**Example**

```js
const [list, setList] = useState([
    {itemNumber: 1, priority: "low", ...},
    {itemNumber: 2, priority: "low", ...},
]);
```

Now that we have `useState` set up, configure your `Table` to display your `list` data. Ensure that the `Table` is properly formatted so that when too many records are added, the table does not expand to be larger than the height of the screen.

Signature `________________________________`

## Part 4: JSON Server & Axios

Now we will add the ability to connect to a `JSON Server` acting as our `API` dynamically providing data records. In order to access the records, we'll need to make `HTTP` requests to certain endpoints (ex. `http://localhost:4000/tasks?_page=2&_limit=20`). We can certainly use `fetch`, a built-in function used to fetch a resource from the network ([fetch on MDN](https://developer.mozilla.org/en-US/docs/Web/API/fetch)), OR we could use a feature rich library to make development that much easier.

Enter `Axios` - `Axios` is a library that manages `HTTP` requests for modern web apps and is a perfect fit for most `ReactJS` applications.

### Setup JSON Server

Let's get started with setting up JSON Server. Install `JSON Server` as a dependency - `npm install -g json-server` (the `-g` flag installs the package globally, not just as a dependency in this project).

Now you can start the JSON Server: `npx json-server -p 4000 --watch db.json`

### Setup Axios

Install Axios as a dependency: `npm install axios`

Create an `axios.js` file to house your main `Axios` instance allowing you to use `Axios` throughout your application. See [Axios Instance](https://axios-http.com/docs/instance)

Once configured properly, you'll be able to make `HTTP` requests similar to below:

```js
const allTasks = await myCoolAxiosInstance.get("/tasks");
```

### Use Axios to connect with JSON Server

To dynamically load our task data, we're going to want to request fresh JSON data whenever our component loads. Luckily, there's a `ReactJS` life-cycle hook for this called `useEffect()`.

`useEffect()` provides the ability to re-run functions whenever a component is re-rendered. In this case, we will want to use `axios` to fetch all database records and save them the local state variable we set up earlier.

See the [useEffect documentation](https://react.dev/reference/react/useEffect) to get started.

Signature `________________________________`

## Part 5: Input New User Tasks

Let's add the ability for user's to manually input new `TODO list` items. For this, we will use a `Dialog` component from `MUI` to create a modal.

See documentation here: https://mui.com/material-ui/react-dialog/

Your mission, should you choose to accept it, is to create a `Button` in your application that, when clicked (hint: `onClick`), opens a popup modal where a user can provide info for a new task.

Your modal should take the following input:

1. Item Number (Number) - Automatically generated based on items in the list
2. Priority (String - High, Medium, Low) - `MUI` `Drop Down list`
3. Item Name - (String) - `Text Field`
4. Item Details (String) - `Text Field`
5. Due Date (Date) - `MUI` `Date Selector`
6. Status (String - Completed, Open) `MUI` `Drop Down list`
7. Created At (Date) - Automatically generated based on current time
8. Updated At (Date) - Automatically generated based on current time

There must be an `Add` `Button` that is at the bottom of the modal to allow users to complete adding the item. This `Button` should be disabled until all necessary data fields are filled with valid data.

Don't forget, we'll want to send new tasks to our JSON Server for safe keeping. So maybe your logic flow looks something like:

```
1. User Clicks Add
2. Axios PUT Request to JSON Server
3. Refresh of JSON data in component triggered

--> Component displays new item
```

Signature `________________________________`

## Part 6: Edit Existing Tasks

Now we will add the ability to edit existing tasks. Let's re-use our existing `MUI` `Dialog`.

Of course, there are many ways to accomplish this and, as an option to get started, you could create a prop that you pass to your existing `Dialog` component specifying the modal's intended use (e.g. `add` or `edit`).

Manipulate the title of the modal to indicate to the user if your `adding` or `modifying` a record.

In addition, when editing a record, you will pass the existing selected data record to the `Dialog` component

The other part to this is providing the user with a method to select a task to edit. A recommendation would be to add a button to each `Table` row that allows a user to `modify` the record. When this is selected, you will load the modal in `edit` mode with the respective record.

Finally, there should be a `Modify` button at the button at the bottom of the modal that allows the user to save changes to the modified record.

By the way, you'll likely need to send a `PATCH` request to your JSON Server to update existing records.

###### Bonus: If you want an extra challenge, keep the `Modify` modal button disabled until a user makes changes to existing task data.

Signature `________________________________`

## Part 7: Deleting Tasks

Now we'll create a `Button` allowing users to delete records.

Lusers make mistakes, so let's ensure they actually intend to delete a task when the `delete` `Button` is clicked. Use a modal to make the user `confirm` their request. In the same modal, a user should be able to hit `cancel` to stop the process.

Be creative with your style here!

Signature `________________________________`

#### Bonus Section - Light / Dark Mode

If you have time, try taking what you've learned in class and creating a light and dark mode for your application!

You should place a toggle switch in the upper right-hand corner of the screen to do this.

## Part 8: Pushing to Git

Once you completed all parts of the lab, and you have a verified working solution, you're ready to commit your progress to Git just like you did in previous labs.

Ensure you're using your project branch:

```shell
  git branch
  master
* my-cool-branch
```

Commit your code:

```shell
git add .
git commit -m "Created a TODO list application in React that interfaces with a JSON server"
git push

# alternatively
git commit -a -m "<my commit message>"
git push
```

Let us know when you've pushed your code to GitLab

Signature `________________________________`

### Congrats! You've completed Lab 7!
