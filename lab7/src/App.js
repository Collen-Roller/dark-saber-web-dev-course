import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h3>Dark Saber Web Course - Lab 7</h3>
        <h4>TODO List</h4>
        <h5>See README.md for instructions :)</h5>
        <a
          className="App-link"
          href="https://gitlab.com/Collen-Roller/dark-saber-web-dev-course/-/tree/main/day3/lab7"
          target="_blank"
          rel="noopener noreferrer"
        >
          Click here to get started!
        </a>
      </header>
    </div>
  );
}

export default App;
