# Welcome to the Self-Paced Learning Guide for HTML and CSS!

This guide is designed to help you learn the basics of web development with HTML and CSS in two hours. It is recommended to spend around 30 minutes on each section, but feel free to adjust work at your pace.

### Documentation Resources

- https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started
- https://www.w3schools.com/html/html_intro.asp

### Online Code Editors (free)

- [GitHub CodeSpaces](https://github.com/codespaces)
  - account required
  - run services and port forward
- [VSCode Online](https://vscode.dev)
  - no command terminal
- [Mozilla PLAYGROUND](https://developer.mozilla.org/en-US/play)
  - no command terminal
- [CodePen](https://codepen.io/pen/)
  - no command terminal

### Recommended learning resource

- [Free Code Camp](https://www.freecodecamp.org/learn)
- [Digital University](https://digitalu.af.mil/) (smart card)

### Prerequisites

**Before you start, make sure you have the following:**

- A text editor (e.g., Visual Studio Code, Notepad++)
- A web browser (e.g., Google Chrome, Mozilla Firefox)

### Tools for Later

- [GIT CLI (command line interface)](https://git-scm.com/downloads)
- [Download Docker Desktop](https://www.docker.com/products/docker-desktop/)

### Table of Contents

- [Getting Started](#getting-started)
- [HTML Basics](#html-basics)
  - [Code Comments](#code-comments)
- [CSS Basics](#css-basics)
- [Styling the Web Page](#styling-the-web-page)
- [Conclusion](#conclusion)

### Getting Started

- **index.html**: This will be the main HTML file.
- **style.css**: This will be the CSS file where you'll add styles to your web page.

- Now, create `index.html` within the `/lab1` folder.
  - Result: `/lab1/index.html`

### HTML Basics

In this section, you'll learn the basic structure of HTML.

HTML (HyperText Markup Language) is the foundation of any web page. It is used to structure the content and layout of a web page. HTML documents are made up of **elements**, which are represented by tags enclosed in angle brackets.
Opening division or 'section' tag `<div>`, closing tag `</div>`.

- Providing the [`<!DOCTYPE html>`](https://developer.mozilla.org/en-US/docs/Glossary/Doctype) declaration on line 1 of your `index.html` file tells the browser that you're using `HTML5` and not legacy html code.
- You will Learn about the `<html>`, `<head>`, and `<body>` tags (aka: elements) which each have a corresponding closing tag `</html>`, `</head>`, and `</body>`.

  **NOTE:** Some tags/elements are "self-closing" (aka: void element) such as an `<img />` or `<input />`.

  - [(optional) Read about void element/self-closing tags](https://developer.mozilla.org/en-US/docs/Glossary/Void_element)
  - [(optional) Read about < img /> tag](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img)
  - [(optional) Read about < input /> tag](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input)

```html
<!DOCTYPE html>
<html>
  <head>
    <!-- Metadata and CSS references go here -->
    <title>My First Webpage</title>
  </head>
  <body>
    <h1>Hello, World!</h1>
    <p>This is a paragraph.</p>
    <!-- Page content goes here -->
  </body>
</html>
```

HTML elements like headings (`<h1>`, `<h2>`, `<h3>` up to `<h6>`), and paragraphs (`<p>`)

h1 being the largest font size provided by an html **header** tag.<br/>
h6 being the smallest font size provided by an html **header** tag.<br/>
<sub>(with CSS you can customize any elements text font size.)</sub>

```html
<h1>This is a Heading</h1>
<h6>This is a smaller Heading</h6>
<p>This is a <strong>bold</strong> and <em>italic</em> text.</p>
```

Links (`<a>`), images (`<img>`), and lists (`<ul>`, `<ol>`, `<li>`).

```html
<!-- "href" is required to make a link work -->
<!-- "target" is an optional property that opens the link in a new page/tab -->
<a href="https://google.com" target="_blank">Google Search</a>
<!-- "src" is required -->
<!-- "alt" is recommended as it provides context what the image is if the image is not found at the source path and cannot be loaded -->
<img src="image.jpg" alt="Description of the image" />
<img
  src="https://m.media-amazon.com/images/I/71+mDoHG4mL.png"
  alt="a cat image from google"
/>
<!-- "ol" is an Ordered List -->
<ol>
  <!-- "li" is a List Item and should exist within either a "ul" or "ol" -->
  <li>Item 1</li>
  <li>Item 2</li>
</ol>
<!-- "ul" is an Unordered List -->
<ul>
  <li>Item 1</li>
  <li>Item 2</li>
</ul>
```

HTML tables - [`<table><table/>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table).

Tables provide the ability to represent data rows. Below is an example of a basic table represented in HTML

```html
<!-- Start of the table -->
<table>
  <!-- Table header section -->
  <thead>
    <!-- Header row -->
    <tr>
      <th>Header 1</th> <!-- Column 1 header -->
      <th>Header 2</th> <!-- Column 2 header -->
    </tr>
  </thead>
  
  <!-- Table body section -->
  <tbody>
    <!-- First data row -->
    <tr>
      <td>Row 2, Column 1</td> <!-- Row 2, Column 1 data -->
      <td>Row 2, Column 2</td> <!-- Row 2, Column 2 data -->
    </tr>
    
    <!-- Second data row -->
    <tr>
      <td>Row 3, Column 1</td> <!-- Row 3, Column 1 data -->
      <td>Row 3, Column 2</td> <!-- Row 3, Column 2 data -->
    </tr>
  </tbody>
</table>
<!-- End of the table -->
```

#### Code Comments

Comments serve as a way to add explanatory notes or remarks within the HTML code that are not rendered on the web page. They are meant for developers and do not affect the appearance or functionality of the webpage for end-users.

The purpose of HTML comments includes:

**Documentation**: Comments provide a means for developers to document their code. They can add notes, explanations, or reminders that help them understand the purpose of specific sections of the code. This documentation is valuable, especially when revisiting the code at a later time or when collaborating with other developers.

**Debugging**: Comments can also be used for debugging purposes. Developers can temporarily comment out sections of code to see how the page behaves without that particular code segment. This helps identify issues and isolate problematic areas.

**Communication and Collaboration**: When working on a project with a team, comments allow developers to communicate with each other. They can provide insights into what a particular piece of code does or explain why a certain decision was made. This improves collaboration and helps team members understand each other's contributions.

**Temporary Changes**: Sometimes developers need to make temporary changes or experimental adjustments to the code. By adding comments, they can keep track of these changes, making it easier to revert back to the original code later.

**Hiding Code**: If a developer wants to hide a section of code without deleting it, they can comment it out. This can be useful when trying different approaches or when saving a piece of code for later use.

##### Code Comment Examples in Web Project Files (Multiple Languages)

_index.html_

```html
<!-- This is an HTML comment. It won't be displayed on the webpage. -->
```

_style.css_

```css
/* This is a css code comment */
```

_script.js_

```js
// This is a JavaScript "single-line" comment

/*
This is a
"multi-line" comment
*/
```

### CSS Basics

In this section, you'll dive into the world of CSS, which stands for **Cascading Style Sheets**. CSS is used to add styles and layout to your web pages.

- Start by creating and linking a `style.css` file to your `index.html` file using the `<link />` tag.
- You will learn about CSS "selectors" and how to target HTML elements for styling.
- Understand the use of properties and values to modify elements' appearance (e.g., color, font-size, margin, padding, and even position).
- Explore CSS `classes` and `IDs` for more precise element targeting.
- Creating a Simple Web Page

Now that you know the basics of HTML and CSS, it's time to create a simple web page!

- Design the structure of your web page using HTML elements.
- Add content to your web page using headings, paragraphs, images, and links.
- Use CSS to style your web page and make it visually appealing.

### Styling the Web Page

In this final section, focus on enhancing the appearance of your web page using CSS.

- Experiment with different font styles and colors.

#### Changing text color using CSS within an HTML `<style>` tags

Note: _Best practice is to keep the `<style>` tag and CSS at the top within the `<head>` tag section, but this is not the only method to apply CSS._

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Changing Text Color</title>
    <style>
      /* Select any and all paragraph (p) tags and set the text color to red */
      p {
        color: red;
      }
      /* Select all headings and set the font family to 'Arial', sans-serif */
      h1,
      h2,
      h3,
      h4,
      h5,
      h6 {
        font-family: "Arial", sans-serif;
      }
    </style>
  </head>
  <body>
    <p>This is a paragraph with red text color.</p>
    <h1>This is a heading</h1>
    <h2>This is another heading</h2>
    <h3>Yet another heading</h3>
    <h5>
      This is an h5 header with a different font family from the paragraphs (p)
    </h5>
  </body>
</html>
```

Add margins and padding to create spacing.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Centering Elements</title>
    <style>
      /* Center the div horizontally and vertically */
      .centered-div {
        width: 200px;
        height: 100px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
      }
      .colored-div {
        background-color: lightblue;
      }
      /* . (dot) is the class selector symbol */
      .container {
        /* border size, color, style */
        border: 1px solid blue;
      }
      /* # is the ID selector symbol */
      #special-box {
        background-color: pink;
        border: 3px green dashed;
        /* padding is internal spacing */
        padding: 25px;
        /* margin is external spacing */
        /* margin: 4rem 2rem; */
        /* margin: 4rem 2rem 4rem 2rem; */
        margin: 4rem;
        /* make the font bold */
        /* font-weight: 900; */
        font-weight: bold;
      }
    </style>
  </head>
  <body>
    <div class="centered-div colored-div">Centered Content</div>
    <div class="container">
      <div id="special-box">This is a special color box</div>
    </div>
  </body>
</html>
```

Explore background images and colors.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Background Example</title>
    <style>
      /* Setting a background color */
      body {
        background-color: #0e3f5f; /* Blue color */
        color: white;
        font-family: Arial, sans-serif;
      }

      /* Adding a background image */
      .container {
        background-image: url("header-background.png"); /* Replace 'header-background.png' with your own image file */
        background-size: auto; /* Auto allows the image to maintain it's own size */
        background-position: top center; /* positions the background image at the top vertically and at the center horizontally within its containing element. */
        background-repeat: no-repeat; /* Prevent image repetition */
        padding: 100px 0px 20px;
        text-align: center;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>Welcome to My Website</h1>
      <p>
        This is a simple example of setting background colors and images in
        HTML.
      </p>
    </div>
  </body>
</html>
```

Using CSS `flexbox` to create a responsive layout.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Flexbox Responsive Layout</title>
    <style>
      body {
        background-color: #0e3f5f;
      }
      /* Flex container styles */
      .container {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
      }

      /* Flex item styles */
      .item {
        width: calc(33.33% - 20px);
        margin: 10px;
        padding: 20px;
        background-color: #3498db;
        color: white;
        text-align: center;
      }

      @media screen and (max-width: 768px) {
        /* Adjust item width for smaller screens */
        .item {
          width: calc(50% - 20px);
        }
      }

      @media screen and (max-width: 480px) {
        /* Adjust item width for even smaller screens */
        .item {
          width: 100%;
        }
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="item">Item 1</div>
      <div class="item">Item 2</div>
      <div class="item">Item 3</div>
      <div class="item">Item 4</div>
      <div class="item">Item 5</div>
    </div>
  </body>
</html>
```

Using CSS `grid` to create responsive items.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Grid Responsive Layout</title>
    <style>
      body {
        background-color: #0e3f5f;
      }
      /* Grid container styles */
      .container {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
        grid-gap: 20px;
        justify-content: center;
      }

      /*
      In CSS Grid layouts, 1fr is a unit of measurement used to define flexible grid tracks. The fr stands for "fractional unit," and it allows you to distribute available space within the grid container among grid tracks (columns or rows) in a flexible way.

      When you specify 1fr for a grid track, it means that it should take up one equal fraction of the available space within the grid container. If you have multiple tracks with different fractions (e.g., 2fr, 1fr, 3fr), the available space will be divided among them according to their fractions. In this example, the track with 3fr will get three times as much space as the track with 1fr.
      */

      /* Grid item styles */
      .item {
        background-color: #3498db;
        color: white;
        padding: 20px;
        text-align: center;
      }

      @media screen and (max-width: 768px) {
        /* Adjust grid for smaller screens */
        .container {
          grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
        }
      }

      @media screen and (max-width: 480px) {
        /* Adjust grid for even smaller screens */
        .container {
          grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
        }
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="item">Item 1</div>
      <div class="item">Item 2</div>
      <div class="item">Item 3</div>
      <div class="item">Item 4</div>
      <div class="item">Item 5</div>
    </div>
  </body>
</html>
```

Different ways to apply CSS

1. Internal CSS _(which you have seen previously)_:\
   With internal CSS, you define the styles directly within the `<style>` element in the `<head>` section of your HTML document. This method is useful when you have a small amount of CSS to apply to a specific page.

1. External CSS File:\
   This method involves creating a separate CSS file and linking it to the HTML document using the `<link>` element. This is the most common and **recommended** way to organize and manage your CSS.

```html
<!-- index.html -->
<!DOCTYPE html>
<html>
  <head>
    <title>External CSS Example</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <h1>Hello, World!</h1>
    <p>Good day to you!</p>
  </body>
</html>
```

```css
/* style.css */
h1 {
  color: blue;
}

p {
  font-size: 16px;
}
```

1. Internal CSS:\
   With internal CSS, you define the styles directly within the `<style>` element in the head section of your HTML document. This method is useful when you have a small amount of CSS to apply to a specific page.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Inline CSS Example</title>
  </head>
  <body>
    <h1 style="color: purple;">Hello, World!</h1>
    <p style="font-size: 20px; padding: 10px;">
      This is a paragraph with some text.
    </p>
  </body>
</html>
```

### Conclusion

Congratulations on completing the learning guide for HTML and CSS! By now, you should have a good understanding of the beginning basics of web development.

Remember, practice and experimentation is key to mastering these skills, so keep building and experimenting with your web pages.

Happy coding! 🚀

Signature `________________________________`
