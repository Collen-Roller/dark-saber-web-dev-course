# Lab 10 - Containerization

## Part 0: Start with your Lab 9 Solution

In this lab, you'll be tasked to create a new directory `day4/lab10/<YOUR_APP_NAME>`

Within this directory, please copy your working `lab9` contents into the directory you just created in `day4/lab10/<YOUR_APP_NAME>`

Take the `docker-compose.yml` file from your `/lab9/api` folder and place it
within your `lab10/<YOUR_APP_NAME>` directory. Your `client` and `api` directories
should be within the same root directory

Your `docker-compose.yml` should look like this:

```yml
version: "3.8"

networks:
  backend:
    driver: bridge

services:
  mongo:
    container_name: mongo
    image: mongo:latest
    networks:
      - backend
    ports:
      - "27017:27017"
    volumes:
      - todolist-vol:/data/db

volumes:
  todolist-vol:
```

The main objective with this lab is to build our entire application into docker images.

To start with this `docker-compose.yml` file will only run the mongo database.

## Part 1: Iron Bank

In this next part, we will show you how to log into PlatformOne's IronBank container registry which stores DoD scanned/secured and approved docker images.

First, go to Iron Bank (https://registry1.dso.mil/).<br>
If you do not have a PlatformOne account, please create one.

Once you have an account, go to Iron Bank's Docker Registry (Registry1) and login using the `ODIC provider` button on the login page. Once you're in, you'll need your username and password that can be found by clicking on your account information in the upper right-hand corner.<br> **This is NOT the same as your PlatformOne account password**.

Once you have your login information, head back to your VS Code session and open a terminal window. You're going to want to login to the PlatformOne Docker Registry (Registry1) in order to pull down containers from the registry to your local machine.

##### Note: if you using GitHub code-spaces at this point, you can login to the registry from the code-space.

In order to login, we will use the `docker` Command-Line-Interface (CLI) tool.

```ps1
docker login registry1.dso.mil --username <YOUR_PASSWORD> --password <YOUR_PASSWORD>
```

### Important Security Notes

In the sections below in this lab, you'll notice that we use Iron Bank docker images as our base images and build the `API` and `Client` applications on top of them. It's extremely important here to pay attention to the versions of the base containers that you are using.

 This is important because quite often, security vulnerabilities are identified and reported resulting in a new version to be released. For example, you'll notice that for `node` we specifically use `nodejs18` version `18.13.0`. At the time of writing this lab, this is the latest version available that passed the Iron Bank criteria and acceptance score threshold. Before deploying your application, it's a good idea to use the latest and most up-to-date docker images from Iron Bank and review and critical security finding that were identified. This is a good habit to create for yourself as a developer in both development and production.

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

## Part 2: API Containerization

In this section, we will containerize your API that you built in `lab8` and used in `lab9`

### Section 1: PM2

To begin we will introduce a 3rd party library called `PM2`. `PM2` is a node
library that is built for running `HTTP` or `Express` based APIs. This
library acts as a software load-balancer for your `API`. When users interact with the client, it's possible that the client might be serving multiple people at one time. When this is the case, occasionally the `API` will get overloaded with requests. We want to ensure no matter what that our clients always have a reliable experience and that `API` requests never fail when the server is busy

To cope this this possibility, we introduce `PM2`, a load-balancing library that can spin up multiple `API` instances to handle larger request loads. `PM2` will automatically scale our application across a larger workload

To setup `PM2`, there are a few steps, first we must install it into our `api` project folder

```
npm install pm2
```

Next, we need to create a configuration file in the `api` folder and name the file `processes.json`

i.e. `day4/lab10/<YOUR_APP_NAME>/api/processes.json`

Within this file, we will define our configuration for `PM2`

###### Example PM2 Config

```json
{
  "apps": [
    {
      "name": "api",
      "script": "server.js",
      "merge_logs": true,
      "max_restarts": 5,
      "instances": 2,
      "max_memory_restart": "300M",

      "env": {
        "PORT": 4000,
        "NODE_ENV": "development",
        "DB_CONNECTION": "mongodb://mongo:27017/?retryWrites=true&w=majority",
        "NODE_TLS_REJECT_UNAUTHORIZED": 0
      },

      "env_production": {
        "PORT": 4000,
        "NODE_ENV": "production",
        "ENDPOINT": "https://todolist.devilops.dso.mil",
        "NODE_TLS_REJECT_UNAUTHORIZED": 1
      }
    }
  ]
}
```

In the above example, we define the following items

##### PM2 Configuration Fields

1. `name`: name of our `api` we are running
2. `script`: name of the file to run in order to start our `API` service
3. `merge_logs`: if we'd like to merge all of the logs across all instances
4. `max_restarts`: if the `api` fails, it will automatically restart XX number of times before stopping
5. `instances`: the number of instances of the `api` to run
6. `max_memory_restart`: the max memory that is reached prior to restarting the API. Note, sometimes docker container resources are limited when hosting in production, this helps control the `api` size as sometimes files are cached / stored within the running docker container.
7. `env`: this is the most important variable. `env` allows you to define environment variables that will be passed to your application during run-time. THis is similar to the variables in `nodemon.json` files. We can create as many of these as we needed. In this example, we have one for `development` and another for `production`

Within the `env` we are defining the following environment variables that can be used within our application

##### Environment Variables

Below are the environment variables defined in the sample file above

Note: Using `nodemon.json` we set some of these up before-hand

i.e.

```json
{
  "env": {
    "NODE_ENV": "development",
    "DB_CONNECTION": "mongodb://localhost:27017/?retryWrites=true&w=majority"
  }
}
```

- `PORT`: This is the port that the API will listen for requests on
- `NODE_ENV`: This is an environment variable the defines wither we are running node in `development` or `production` mode. Within our application, we can use these environment variables using `process.env.VARIABLE_NAME` to change how our application functions depending on the mode. For example, say we want to change our database connection endpoint?
- `DB_CONNECTION`: This endpoint tells us where our MongoDB is located. Note: you won't see this variable defined for `production` this is on purpose. For production, **NEVER save any type of login information within your code-base despite if it's a private repository**. For `production`, we use `AWS Secret Manager` which manages all login and secret information that we want to be hidden at all times. In a future lab, we will integrate `AWS Secret Manager` into our `TODO List` application. For now, we will be running `development` mode within our `Docker` image that we are building for our `API`
- `NODE_TLS_REJECT_UNAUTHORIZED`: This variable tells Node how to handle TLS connection. Transport Layer Security (TLS) is used for securing your `api` calls by only allowing encrypted in-bound traffic. `NODE_TLS_REJECT_UNAUTHORIZED=0` means that we are not requiring encryption which means that we can use `http` when sending requests to the `api` wheather from a browser or `Postman`. `NODE_TLS_REJECT_UNAUTHORIZED=1` means that we will expect all incoming `api` requests to be passed over `https`. Since we are running our application locally using `http`, we will be using the `development` mode

For more information on `PM2`, visit the `PM2` documentation page (https://pm2.keymetrics.io/)

##### Using PM2

Once you have `PM2` installed and have a `processes.json` file setup, you can now use `PM2` with your `api` application. To automate this, we will create 2 additional script entries within the `/api` directory in  `package.json` file under the **`scripts`** section

Add the following two script entires to the existing command list
```json
scripts: {
	"pm2-prod": "pm2 start processes.json --no-daemon --env production"
	"pm2-dev": "pm2 start processes.json --no-daemon",
	... other scripts
}
```

You can now use the `npm run pm2-dev` to run `PM2` in `development` mode!

### Section 2: API Dockerfile

Next, we will create a `Dockerfile` in the `/api` folder. a `Dockerfile` is a set of instructions that are provided to the Docker Engine in order to build a docker image. Remember a docker image is a saved virtual machine that can be run using the Docker Engine

When a docker image runs, a container is provisioned for the image. The container is a running instance of the virtual machine and the image is the saved virtual machine

Below, is the `Dockerfile` that you can use for your API.

```docker
# Set up arguments to use
# in this case, we are defining arguments to use the Docker Registry
# provided by PlatformOne through IronBank / Registry1
ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/opensource/nodejs/nodejs18
ARG BASE_TAG=18.13.0

# Set Base Image (FROM)
FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

# Set WORKDIR
WORKDIR /app

# Set APP HOME to location within docker container
ENV APP_HOME=/app

# Copy local files to docker container
# Note - you might have to modify this part as you might not have all
# of the same files. If the file doesn't exist, you'll receive an error
COPY models ${APP_HOME}/models
COPY routes ${APP_HOME}/routes
COPY utils ${APP_HOME}/utils
COPY server.js ${APP_HOME}/
COPY package.json ${APP_HOME}/
COPY package-lock.json ${APP_HOME}/
COPY processes.json ${APP_HOME}/

# Set USER to root to allow installation
USER root

# Install dependencies (node modules)
# Note: For this lab, we are building a in normal mode
# For our client, we will build a production instance of our
# client application which will run through a compiler that will
# optimize our code
RUN npm install

# Expose Port
EXPOSE 4000

# Run Script (defined in package.json)
# Is executed each time the image is ran inside a Docker container
# Note - For actual production, we would run the pm2-prod command
CMD ["npm", "run", "pm2-dev"]
```

### Section 3: Adding the API to Docker Compose

Now that our configuration is setup, the next step is to add our `API` to the `docker-compose.yml` file. This will allow `docker-compose` to handle building and managing the execution of all of the containers for the project.

Below is a sample `docker-compose.yml` file that contains both an `api` service and `mongo` database service.

```docker
version: "3.8"

networks:
  backend:
    driver: bridge

services:
  api:
    container_name: api
    image: darksaber/todolist-api:latest
    build:
      context: ./api
    networks:
      - backend
    restart: unless-stopped
    depends_on:
      - mongo
    ports:
      - 4000:4000
    links:
      - mongo

  mongo:
    container_name: mongo
    image: mongo:latest
    networks:
      - backend
    ports:
      - "27017:27017"
    volumes:
      - todolist-vol:/data/db

volumes:
  todolist-vol:
```

When you look at this file, there are 3 new aspects that you likely haven't seen before within the new API service

1. `build`: Build allows us to define a path to a local `Dockerfile`. If the specified image does not exist when `docker-compose` runs, the image will automatically be built. The `context` key specifies the path to look for the `Dockerfile`. In this case, we are looking in the `api` folder which is relative to the `docker-compose.yml` file.
2. `depends_on`:
3. `links`: Links allow you to create a networked link between 2 containers. This is an amazing aspect to Docker. Docker allows you to create Virtual Private Networks (VPNs) across containers.` Docker-compose` makes it easy to handle these situations. For network savvy people, docker VPNs provide a Domain Name Server (DNS) that resolves host names when referenced. This means that I can refer to the `mongo` container from within the `api` container by using the domain name of the `mongo` container. The domain name is set to the `container_name` by default making the `mongo` container accessible through the domain name - `mongo`. As you see in the `docker-compose` segment above, we specify a link to the `mongo` service from the `api` service configuration

#### Modify Your Connection String

Depending on how you created a connection to MongoDB, we either created a mongo cluster at https://mongodb.com or you ran a mongo docker image in a docker container on your local machine. For this lab, we will be requiring you to use a `mongodb` docker image

In your code, there is a method that is connecting your `API` to `Mongo`. You should be using a 3rd party library called `mongoose` to do so. Specifically, you use `mongoose` to connect to your mongo instance by using the following code below. Note: You should already have some type of mongoose connection code setup, you're welcome to use your own or the segment below

##### Note: We normally create a `db.js` file where we put this code

```js
const mongoose = require("mongoose");
const path = require("path");

const mongoConfig =
  process.env.NODE_ENV === "production"
    ? {
        // Production
        tls: true,
        tlsAllowInvalidHostnames: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      }
    : {
        // Development
        useNewUrlParser: true,
        useUnifiedTopology: true,
        // useCreateIndex: true,
        // useFindAndModify: false,
      };

const connectDB = () => {
  try {
    mongoose.connect(process.env.DB_CONNECTION, mongoConfig);
    console.log(`MongoDB Connected -> (${process.env.DB_CONNECTION})`);
  } catch (error) {
    console.error(error.message);
    // Exit process with a failure
    process.exit(1);
  }
};

module.exports = connectDB;
```

Once you have this setup, you'll need to create an environment variable called `DB_CONNECTION` that you can reference. In our case, we have `processes.json` and `PM2` defining environment variables into our application. Specifically, you'll notice that we reference the `mongo` DNS entry from `processes.json` where we set the `"DB_CONNECTION": "mongodb://mongo:27017/?retryWrites=true&w=majority"`

### Section 4: Building API

Once you have all of this setup, you're ready to test it. There are a few ways to build your docker image that we will review before launching docker containers.

1. Building docker images from the command line using the Docker Command Line Interface (CLI). To do this, we will `cd` into the `api` directory, and run the following

```ps1
docker build -f Dockerfile -t todolist:latest .
```

This command tells `docker` to use the `Dockerfile` in the current directory and build from the context of the current directory. The `-t` denotes to tag the image with `todolist:latest` for later reference

2. Building docker images using `docker compose` is far easier than building the docker via command line. We already setup the `docker-compose.yml` file to automatically build if the image does not already exist on the host machine.

Note: If you ever want to rebuild the docker images that are referenced within the `docker-compose.yml` file (services) you can simple pass the `--build` flag at the end

i.e. `docker compose up --build`

### Section 5: Testing the API Container with MongoDB

Once you've gotten this far, you should test your docker images out! To do this, from your projects root directory, run `docker-compose up` which will spin up the images into containers. You should have a `mongo` database hosted on 27017 and your `api` hosted on `4000`. Your API should be connected to `mongo` using the internal docker network.

To test, try using `Postman` to interact with the API and see if it works properly.

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

## Part 3: The Client

In this part of the course, we will be adding our `Client` to our docker configuration. In order to build a production ready `ReactJS` application, we will use NGINX!

### Section 1: NGINX

`NGINX` is a open-source tool build for hosting web servers. For our use-case, we are using `NGINX` as a reverse-proxy! Reverse proxy allow us to allow clients to pass a request to themselves and the NG``INX reverse-proxy will redirect these requests to an API. This re-routing that happens is referred to as reverse proxying

Take this example to understand how reverse proxying works

1. User accesses Client at `http://localhost:3000`
2. Client wants to make a call to fetch data from an API, however the API available at `http://localhost:4000` is not publicly available
3. Client instead makes a request to `http://localhost:3000/api` where all requests that go to `/api` are redirected to `http://api:4000` which is only visible to the docker container running the client
4. The request is sent to the `api` hosted at `http://api:4000` to fetch the data, and is returned back to the client

What this achieves is a single point of access vs having an API hosted on a completely separate domain

To checkout more - read up on `NGINX` here: https://www.nginx.com/

For this section, we will be creating a reverse-proxy configuration for NGINX to use that we will store in our `client` and copy over into the production `docker` when the image is building

1. Create a `nginx` directory within the `client` directory of your `lab10`

2. Within this, create a default `NGINX` configuration file called `default.conf`.

3. Copy the contents of the file below into it

```nginx
upstream proxy_api {
    server api:4000;
}

server{
    listen 80;

    location / {
        root /usr/share/nginx/html;
        index index.html index.htm;
        try_files $uri $uri/ /index.html;
    }

    location /api {
        try_files $uri @proxy_api;
    }

    location @proxy_api {
        proxy_set_header X-Url-Scheme $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_pass http://api:4000;
    }
}

```

##### NGINX default.conf

NGINX will use this `default.conf` configuration every time the `docker` image runs.

Within this file, we define 2 core attributes, the `server` and the `upstream proxy`. The `server` contains information about how the `NGINX` should run, such as what `port` will it `listen` on (80), that files should it share (complied web application that we will copy into the `NGINX` container), and how we handle `api` traffic

###### Note: `api` is the name of our `api` docker container within our `docker-compose.yml` file! This allows us to reference that DNS entry within the NGINX image that will be hosting our web application

`NGINX` is a large web-server that offers a huge set of hosting options. We are using a very simple configuration in this instance. It's worth checking out other tutorials and videos to learn more about it. It's a very versatile web-server that's used everywhere

### Section 2: Client Dockerfile

Next, we will create a Dockerfile for the `client`. Again, a Dockerfile is a set of instructions that are provided to the Docker Engine in order to build a docker image. Remember a docker image is a saved virtual machine that can be run using the Docker Engine

Below, is the Dockerfile that you can use for your `client`

```yml
# Set up arguments to use
# in this case, we are defining arguments to use the Docker Registry
# provided by PlatformOne through IronBank / Registry1
ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/opensource/nodejs/nodejs18
ARG BASE_TAG=18.13.0

# Set Base Image (FROM)
# Note: Here we are using docker stages, check out more about this
# Stage 1 will be to build our web-app and stage 2 will be
# Stage 2 will be to use out compiled web app and host it using
# NGINX
FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG} AS build
LABEL stage=build

# Set WORKDIR
WORKDIR /app

# Set APP HOME to location within docker container
ENV APP_HOME=/app

# Copy local files to docker container
# Note - you might have to modify this part as you might not have all
# of the same files. If the file doesn't exist, you'll receive an error
COPY public ${APP_HOME}/public
COPY src ${APP_HOME}/src
COPY nginx ${APP_HOME}/nginx
COPY package.json ${APP_HOME}/package.json
COPY package-lock.json ${APP_HOME}/package-lock.json

# Set USER to root to allow installation
USER root

# Install dependencies (node modules)
# Note: For this lab, we are building a in normal mode
# For our client, we will build a production instance of our
# client application which will run through a compiler that will
# optimize our code
RUN npm install

# Builds production instance - this maps to a NPM build command
# defined in the package.json
RUN npm run build


# Stage 2 of Build - NGINX
# This copies the files from the Node container into the NGINX
# container for hosting
FROM registry1.dso.mil/ironbank/opensource/nginx/nginx:1.23.2

# WORKDIR for the hosted NGINX files
WORKDIR /usr/share/nginx/html

# Copy files from Node to NGINX container
COPY --from=build /app/build /usr/share/nginx/html
COPY --from=build /app/nginx/default.conf /etc/nginx/templates/default.conf.template

# Run Script - hosted using the NGINX CLI
CMD ["nginx", "-g", "daemon off;"]
```

### Section 3: Adding the Client to Docker Compose

In the next section, we will be adding the client to our `docker-compose.yml` file. To do this, we will perform two core actions. 1. We will add a `frontend` network driver to handle all the network between our host machine and the client. The `backend` network will be connected to our client and that is it. The `backend` will contain a `mongo` instance and `api` and a connection to the `client` while the `frontend` will only be connected to the `client`.

```docker
version: "3.8"

networks:
backend:
driver: bridge
frontend:
driver: bridge

services:
api:
container_name: api
image: darksaber/todolist-api:latest
build:
context: ./api
networks: - backend
restart: unless-stopped
depends_on: - mongo
ports: - 4000:4000
links: - mongo

mongo:
container_name: mongo
image: mongo:latest
networks: - backend
ports: - "27017:27017"
volumes: - todolist-vol:/data/db

client:
container_name: client
image: darksaber/todolist-client:latest
depends_on: - api
networks: - frontend - backend
ports: - 3000:80
restart: unless-stopped
links: - api

volumes:
todolist-vol:

```

##### Source Build

Before we run `docker compose up`, a large consideration when building production web applications is hiding the source code. Within `ReactJS` we specifically can provide a source code mapping when we create our production build. This allows users to see the source code of the application within the browser! This is **not** ideal! To remove the source and only share the built source code, we will add an environment variable letting ReactJS know when it's building to **not** share the source. To do this, add `GENERATE_SOURCEMAP=false` to the front of our `build` command located within the `package.json` file. See below.

```json
"build": "GENERATE_SOURCEMAP=false react-scripts build"
```

#### Build it!

When you run `docker-compose up` any images that are not yet built should build for you :)

Note: Building a production ReactJS app is SLOW! So be patient!

Ensure the application is working as expected on 3000!

### Final Notes - Reverse Proxying:

So your solution **SHOULD** work. However, it likely own't be using the reverse-proxy because you're likely still hitting `localhost:4000/api` directly for the `API`

In order to get the reverse proxy to work correctly, we will be hitting our `api` using the `client` endpoint (`http://localhost:3000`) at `/api`. This will redirect our request

Since both the `backend` and `frontend` are set to `bridge` mode in the `docker-compose.yml`, you should be able to hit the `api`, `mongo` instance and `client`. However, in practice, we normally only expose the `clients` network in bridge mode so we only share a the port which the `client` is hosted on

To test the reverse proxy, go to your axios config, and ensure that `localhost:3000/api` is your `baseURL` (notice the port number is `3000` NOT `4000`)

```js
const instance = axios.create({
	// baseURL: "http://localhost:4000/api",
	baseURL: "http://localhost:3000/api",
	timeout: 3000,
});
```

Once you do this, stop your `docker compose` by running the comman `docker compose down`, and run `docker-compose up --build` which will rebuild all of your containers.

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

## Part 4: Pushing to Git

Once you completed all parts of the lab and you have a verified working solution, and push your progress to Git!

Let us know when you've pushed your code to GitLab

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

#### Congrats - You've dockerized your first full stack application and completed Lab 10!