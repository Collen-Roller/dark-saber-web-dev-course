
export enum HornStyles {
    WIMPY,
    STANDARD,
    PRETTY_LOUD,
    WHOA,
}

export class CarHorn {

    private hornStyle: HornStyles;

    constructor(hornStyle: HornStyles) {
        this.hornStyle = hornStyle;
    }

}