# Lab 2 - Building a Simple HTML Form

In this tutorial, we'll create a simple HTML form for "contacting us". The form will include fields for the user's first name, last name, age, email, and a message.

### Prerequisites

Before you begin, make sure you have a basic understanding of HTML, CSS, and JavaScript. Also ensure you've completed lab 1 :)

### Step 1: Set Up Your HTML Document

Create a new HTML file, e.g., `index.html`, and add the following code to set up the basic structure of your document:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Simple Form Example</title>
  </head>
  <body>
    <!-- Your form content will go here -->
  </body>
</html>
```

In this code, we've set the document's character encoding, added a viewport meta tag for responsiveness, and linked external styles and scripts (CSS and JavaScript) that we'll create later.

### Step 2: Create the Form Structure

Inside the `<body>` tag, create a container for your form using a `<div>` element with the class "container." Then, add a heading and a `<form>` element with an id of "contact-form" for the form:

```html
<div class="container">
  <h2>Contact Us</h2>
  <form id="contact-form">
    <!-- Form fields will go here -->
  </form>
</div>
```

### Step 3: Add Form Fields

Inside the `<form>` element, add the form fields you want to include. For this example, we'll add fields for first name, last name, age, email, and a message:

```html
<div class="form-group">
  <label for="first_name">First Name:</label>
  <input type="text" id="first_name" name="first_name" required />
</div>
<div class="form-group">
  <label for="last_name">Last Name:</label>
  <input type="text" id="last_name" name="last_name" required />
</div>
<div class="form-group">
  <label for="age">Age:</label>
  <input type="number" id="age" name="age" min="1" max="99" />
</div>
<div class="form-group">
  <label for="email">Email:</label>
  <input type="email" id="email" name="email" required />
</div>
<div class="form-group">
  <label for="message">Message:</label>
  <textarea
    id="message"
    name="message"
    required
    rows="5"
    width="100%"
  ></textarea>
</div>
```

Here, we've used `<label>` elements to describe each input field, and we've set the "required" attribute for fields that must be filled out.

### Step 4: Add a Submit Button

Include a submit button at the end of the form:

```html
<div class="form-group">
  <button type="submit">Submit</button>
</div>
```

### Step 5: Create a Success Message Area

Below the form, create a `<div>` element with the class "success-message" and an `id` of "success-message" where you can display a success message later via JavaScript:

```html
<div class="success-message" id="success-message"></div>
```

### Step 6: Add JavaScript Functionality

Finally, include a script tag at the end of the `<body>` section to link your JavaScript file, which will handle form submissions and displaying success messages:

```html
<script src="script.js"></script>
```

Create a new JavaScript file, e.g., `script.js`, and add the following code.

```js
// JavaScript to handle form submission and display a success message
const form = document.getElementById("contact-form"); // Get the form element by its id
const successMessage = document.getElementById("success-message"); // Get the success message element by its id

// Add an event listener to the form for when it is submitted
form.addEventListener("submit", function (e) {
  // Prevent the default form submission behavior, which would reload the page
  e.preventDefault();

  // Create a FormData object to collect the form data
  const formData = new FormData(form);

  // Get specific form fields by their "name" attributes using the FormData object
  const fname = formData.get("first_name"); // Get the value of the "first_name" field
  const lname = formData.get("last_name"); // Get the value of the "last_name" field
  const age = formData.get("age"); // Get the value of the "age" field
  const email = formData.get("email"); // Get the value of the "email" field

  // You can perform further processing here, e.g., sending data to a server.
  console.log(fname, lname, age, email); // Output the form data to the browser console

  // For this example, we'll display a success message to the user. (Template Literals)
  successMessage.innerText = `Thank you, ${fname} ${lname}!\rYour message has been sent to ${email}.`; 

  // Reset the form fields to their initial state
  form.reset();
});
```

#### String Reprenstation

##### Template Literals 

Template literals are represented in the code above as `${fname} ${lname}`. These are variables that can be referenced within a string. However, this is a specific type of string that is represented using ` (backticks) vs " (double quotes) or ' (single quotes). In order to reference variables within strings in JavaScript, you MUST use backticks.

```js
successMessage.innerText = `Thank you, ${fname} ${lname}!\rYour message has been sent to ${email}.`; 
```


##### String Concatenation
In addition to using template literals, you can optionally use string concatenation to format multiple strings together

```js
successMessage.innerText = "Thank you, " + fname + " " + lname + "\n" + "Your message has been sent to " + email
```


### Step 7: Style Your Form

For styling, you can create a separate CSS file (e.g., `style.css`) and add your CSS style classes to it. You can also modify the existing styles in the provided code.

To do this, include a link tag at the end of the `<head>` section (between the open and close tags `<head>` `</head>`) to link your CCS file, which will handle custom styling for your webpage:

```html
<link rel="stylesheet" href="style.css" />
```

Create a new CSS file, e.g., `style.css`, and add the following code.

```css
/* Set the font family and background color for the entire page */
body {
  font-family: Arial, sans-serif;
  background-color: #0e3f5f;
}

/* Style the main container for the form */
.container {
  max-width: 400px;
  margin: 0 auto; /* Center the container horizontally */
  padding: 20px;
  background-color: #fff;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.2); /* Add a shadow to the container */
}

/* Apply specific styling to the contact form within the container */
#contact-form {
  padding-right: 20px;
}

/* Style for labels used in the form */
label {
  display: block; /* Display labels as blocks */
  margin-bottom: 5px; /* Add spacing below labels */
}

/* Style for text inputs, number inputs, email inputs, and textarea */
input[type="text"],
input[type="number"],
input[type="email"],
textarea {
  width: 100%; /* Make input fields span the entire width */
  padding: 10px; /* Add padding to input fields */
  margin-bottom: 10px; /* Add spacing between input fields */
  border: 1px solid #ccc; /* Add a border to input fields */
  border-radius: 5px; /* Round the corners of input fields */
}

/* Style for labels within form groups */
.form-group label {
  position: relative; /* Set label position relative for proper alignment */
  width: fit-content; /* Make label width fit its content */
}

/* Add a red asterisk before labels for required fields */
.form-group:has(:required) label:before {
  content: "*"; /* Add asterisk symbol */
  font-size: 25pt; /* Adjust the font size of the asterisk */
  color: red; /* Set asterisk color to red */
  position: absolute; /* Position the asterisk absolutely */
  top: -3px; /* Adjust vertical position */
  right: -15px; /* Adjust horizontal position */
  z-index: 1; /* Ensure the asterisk appears above the label text */
}

/* Allow vertical resizing of textareas */
textarea {
  resize: vertical; /* Enable vertical resizing */
}

/* Style the submit button */
button {
  background-color: #3498db; /* Set button background color */
  color: #fff; /* Set button text color to white */
  padding: 10px 20px; /* Add padding to the button */
  border: none; /* Remove button border */
  border-radius: 5px; /* Round the corners of the button */
  cursor: pointer; /* Change cursor to a pointer on hover */
}

/* CSS styling for the success message */
.success-message {
  background-color: #2ecc71; /* Set success message background color */
  color: #fff; /* Set success message text color to white */
  padding: 10px; /* Add padding to the success message */
  margin-top: 10px; /* Add spacing above the success message */
  text-align: center; /* Center-align the success message text */
}
```

#### Congrats! You've completed Lab 2!

<br/>
Signature `________________________________`
