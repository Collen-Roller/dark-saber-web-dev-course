# Lab 8 - Express API + Mongo + Postman

In lab 7, we built a `TODO list` application!

Now, we will build a backend data service, an API (Application Programming Interface), that will eventually bridge our frontend application to our MongoDB in order persist user data.

Overall, we will learn / re-enforce the following concepts:

1. How to set up a MongoDB with Docker and MongoDB Compass
2. Creating our first Express API
3. Use Nodemon for Hot-Reloading
4. Create a Data Model using Mongoose ORM (Object Relational Model)
5. Create Routes that allows us to Create, Update, and Delete records
6. Create Postman routes to test our API

## Part 0: Start with the Base

To begin, `cd` into the `lab8-base` project

Install the following dependencies: `cors`, `express`, and `mongoose` via the command `npm install <PACKAGE NAME>`

You are now able to run the app using `npm start`

In addition, you'll need `Docker Desktop` installed for this exercise. Ensure docker is **installed and running** by going to a `Terminal`/`CLI` and running the `docker --version` command.

## Part 1: Setting up MongoDB + MongoDB Compass

### Get MongoDB Running

Let's set up `MongoDB` by creating a `Docker` container to run, virtualized, on our local machine.

Based on the lecture, you should have some idea of how to build a `docker-compose.yml` file. See our random [docker-compose-example.yml](docker-compose-example.yml) if you're lost.

First, make a new service called `mongodb`.

Within that service, setup the following:

**Classroom Students**: Run `docker load --input ./mongo.tar` -- This will load a copy of the mongoDB image into a local cache, so you don't tie up our limited internet.

1. Make the image reference `mongo:latest` (these images typically pull from [hub.docker.com](https://hub.docker.com/_/mongo))
2. Create a network that is in `bridge` mode. Add the network to the `mongodb` service. ([networks docs](https://docs.docker.com/compose/compose-file/06-networks/))
3. Create a volume called `todolist-vol`. Mount `todolist-vol` to `db/data` under the `mongodb` service
4. Expose port `27017` and map it to `27017` on the host machine

Once you have the file properly configured. Run the `docker compose` command while in the same directory as the `docker-compose.yml` file. To start the containers, run `docker compose up`

### Setup Mongo Compass

Next, install `Mongo Compass` which is a `UI` tool to view `mongodb` records.

**Classroom Students**: This will already be installed for you \
Download the latest version: https://www.mongodb.com/try/download/compass

Now open the `Mongo Compass` and connect to our `MongoDB` instance using this connection string `mongodb://localhost:27017?retryWrites=true&w=majority`

Once you have your database up and running. Call us over to verify.

Signature `________________________________`

## Part 2: Express API

Now we will create an `API` using `ExpressJS` and connect it to the running `MongoDB` instance.

### Setup nodemon _...yea mon!_

Let's use `nodemon` to help us in development. We can install is as a `dev devepency` with:

```shell
  npm install --save-dev nodemon
```

Now, head to the `package.json` file and add an entry under `scripts` to create our own run command that will start `nodemon` for us `nodemon --inspect server.js`.

`nodemon` will look at `server.js` along with all referenced files for changes and will `hot-reload` every time you save a file.

For example (within `package.json`):

```json
{
  "scripts": {
    "start": "node server.js",
    "debug": "nodemon --inspect server.js"
  }
}
```

In your project's `root` directory, create a new file called `nodemon.json`. For now, we'll list a single environment variable within this file. These variables can be used throughout our project

`nodemon.json`

```json
{
  "env": {
    "NODE_ENV": "development"
  }
}
```

Now you can run your application by running `npm run debug` which will run `nodemon` and look for any project changes.

## Part 3: Connecting to Mongo

Alright, now that your `Express API` is live, we will now connect it to your MongoDB. For this, create a `utils` folder within `src` that will contain a file `db.js` that defines your connection to `mongo`.

This database connection uses the npm package `mongoose`, it's not yet installed as a dependency. You should install that now.

```
npm i mongoose
```

Below is a code snippet that you can use in `/api/utils/db.js`:

```javascript
const mongoose = require("mongoose");
const path = require("path");

// Note - Do not use this in production setting
// Should use pem file for security with TLS/SSL
// Dev Only
const mongoConfig = {
  // Non Production
  useNewUrlParser: true,
  useUnifiedTopology: true,
  // useCreateIndex: true,
  // useFindAndModify: false,
};

const connectDB = async () => {
  try {
    await mongoose.connect(process.env.DB_CONNECTION, mongoConfig);
    console.log(`MongoDB Connected -> (${process.env.DB_CONNECTION})`);
  } catch (error) {
    console.error(error.message);
    // Exit process with a failure
    process.exit(1);
  }
};

module.exports = connectDB;
```

Notice the `process.env.DB_CONNECTION` reference. This will reference the same connection string we used before when we connected to `mongo` in `Mongo Compass`. To do this, we're going to define `DB_CONNECTION` as an `environment` variable in another file. There are a few ways to do this, however `nodemon` comes with a pretty simple method for us to achieve this.

Head to your project's `nodemon.json` file that we set up in `Part 2` and add a new environment variable called `DB_CONNECTION`. Give it this string `mongodb://localhost:27017?retryWrites=true&w=majority`

**You'll need to restart your node application for the environment variable to be propagated.**

Finally, import the `db.js` file into `server.js` and call the `connectDB` function.

Once `nodemon` rebuilds/executes your new code, you should see that your application is connected to `mongo` and the following string output to your terminal:

```
MongoDB Connected -> (mongodb://localhost:27017?retryWrites=true&w=majority)
```

Once you're connected, call us over to check your work!

Signature `________________________________`

### Start building ExpressJS Server

See documentation at https://expressjs.com/en/guide/routing.html

Within `server.js` use the code below to create your first `Express` application

```javascript
const express = require("express");
const app = express();
const cors = required("cors");
const connectDB = required("./utils/db");
// call the function to open a connection to the mongo database
connectDB();

// parse the request and make req.body available to the routes
app.use(express.json());
// setup server to browser security to allow requests from any source
// later this will need to be changed to a specific url when in production
app.use(cors({ origin: "*" }));
// Set up port to listen

const PORT = process.env.PORT || 4000;
app.listen(PORT, async () => {
  try {
    console.log(`API server listening on port ${PORT}`);
  } catch (error) {
    console.error("Error in initializing express application", error);
    process.exit(-1);
  }
});
```

Upon saving `server.js` with the above code block, `nodemon` should recognize the change, execute your new script, and you should have an `express` server running on port `4000`.

Did it not work? Maybe you need to install the `express` npm package as a dependency... how'd we do that again?

Now add a route with the code snippet below:

```javascript
app.get("/", (req, res) => {
  res.send("Hello From TODO List");
});
```

Once this is done, head to http://localhost:4000 on your web browser and call us over to check it out!

Signature `________________________________`

## Part 4: Building Routes

Create a new folder within the root directory of the project called `routes`. Within this directory, create a new file called `todo.js` which will hold all routes we will implement for the `TODO List` application.

Initialize `./routes/todo.js` with the code block below:

```javascript
const express = require("express");
const router = express.Router();

/** EXAMPLE ROUTE - Returns all TODO tasks */
router.get("/", async (req, res) => {
  try {
    // Your code to fetch TODO tasks here
    // ** This does not talk to the databse yet **
    const todoItems = [
      { _id: "f78ds6asd67fds8", todoLabel: "Clean house", complete: false },
    ];
    res.send(todoItems);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("500: Server error fetching items");
  }
});

module.exports = router;
```

Once you create your router, you'll need to register it with your `express` application. To do this, head back to `server.js` and add the following:

This code snippet needs to be BEFORE the `app.listen(()=>{...})` section in `server.js`

```javascript
// Add TODO Router to the Express App
app.use("/api/todo", require("./routes/todo"));
```

> Read the rest of Part 4 before starting to build out the routes below

Your task is to create the following routes:

1. `GET` route to return all records in the database
2. `GET` route to return certain records in the database based on an `id` parameter
3. `POST` route that adds a single `TODO List` item to the database
4. `PATCH` route that will update a single `TODO List` item to the database based on an `id` parameter
5. `DELETE` route that will delete a single `TODO List` item to the database based on an `id` parameter

Since `mongoDB` is a non-relational database, we _should_ manage data integrity on the application side, so the database always holds data in a format we expect. To do this, we will make a `mongoose` schema.

Per the docs

> Mongoose provides a straight-forward, schema-based solution to model your application data. It includes built-in type casting, validation, query building, business logic hooks and more, out of the box.

That's nice, less work for us as developers!

To create a `mongoose` schema, start by creating a `models` folder in the root directory of the project. Create a file called `Todo.js` to house your new schema.

```javascript
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
  // NOTE: _id is not in the schema because it is generated by the database automatically when you create/post a new record
  todoLabel: {
    type: String,
    required: true,
  },
  complete: {
    type: Boolean,
    required: true,
    default: false,
  },
  // ...the rest of your schema here
});

module.exports = Todo = mongoose.model("todoList", TodoSchema);
```

Now we can easily validate and save new records in our `./routes/todo.js` router after importing `Todo` with the following:

```javascript
// this goes inside a "router.get("/", async (req, resp)=>{...})" function.
const newEntry = await Todo.create({ property1: "My Cool String" });
res.send(newEntry);
```

### ENTIRE ROUTE

```js
/** EXAMPLE ROUTE - Returns all TODO tasks */
// this belongs to the parent route of "/api/todo"
router.get("/", async (req, res) => {
  try {
    const newEntry = await Todo.create({ property1: "My Cool String" });
    res.send(newEntry);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("500: Server error fetching items");
  }
});
```

See documentation at https://mongoosejs.com/ for more.

### Part 4b: Testing with Postman

To test your routes, you'll want to use `Postman` like we did in class. `Postman` allows you to organize your `API` testing process.

In `Postman` create a new `Collection` and call it `TODO App`. Within this `Collection` add a `Request` for each of the routes that you're creating.

For example - The following request should return all items in the database:

```
  [GET] http://localhost:4000/api/todo/
```

Ensure that you write proper tests for your API and that it is working correctly for each route that you build!

Once you have your `Postman` `Collection` completed, export it and place it in the root directory of the project.

Let us know when you're ready to show us your `API` and `Postman` tests.

Signature `________________________________`

## Part 5: Pushing to Gitlab

Once you completed all parts of the lab, and you have a verified working solution, you're ready to commit your progress to Git just like you did in previous labs.

Let us know when you've pushed your code to GitLab.

Signature `________________________________`

### Congrats - You've made your first API, and you've competed Lab 8!
