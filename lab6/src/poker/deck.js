export function Deck() {
    const vals = [
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "T",
      "J",
      "Q",
      "K",
      "A",
    ];
    const suits = ["S", "D", "H", "C"];
    let deck = [];
  
    function shuffle(array) {
      var m = array.length,
        t,
        i;
  
      // While there remain elements to shuffle…
      while (m) {
        // Pick a remaining element…
        i = Math.floor(Math.random() * m--);
  
        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
      }
  
      return array;
    }
  
    for (const suit of suits) {
      for (const val of vals) {
        deck.push(`${val}${suit}`);
      }
    }
  
    return shuffle(deck);
}
  