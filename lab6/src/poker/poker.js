import {
    getNumberFromValue,
    isWinnerPlayerOne,
    getHandRank,
  } from "./helper.js";
  import { HandRankings } from "./types.js";
  
  // Break out the rawHands
  const computerWinner = (twoHands) => {
    const cardStrings = twoHands.split(" ");
    const handDetails = [];
  
    // Grab the 5 cards for each hand
    for (let i = 0; i < 2; i++) {
      // Save the hands
      const cards = [];
  
      // Grab each card for this hand
      for (let j = 0; j < 5; j++) {
        // Grab the card
        const thisCardString = cardStrings[i * 5 + j];
  
        // Store this card
        cards.push({
          value: getNumberFromValue(thisCardString[0]),
          suit: thisCardString[1],
        });
      }
  
      handDetails[i] = {
        hand: cards,
      };
  
      // Sort the hand
      handDetails[i]["hand"].sort((a, b) => {
        return b.value - a.value;
      });
    }
  
    const hands = {
      player_1: handDetails[0],
      player_2: handDetails[1],
    };
  
    return hands;
  };
  
  // Returns the play who won
  export const findWinner = (twoHands) => {
    let hands = computerWinner(twoHands);
    let count = 1;
    let primaryRanks = [];
    for (const handDetails of Object.values(hands)) {
      // Store the hand rank
      handDetails.handRank = getHandRank(handDetails.hand);
      primaryRanks.push(
        `${count} with ${HandRankings[handDetails.handRank.primaryRank]}`
      );
      count++;
    }
    if (isWinnerPlayerOne(hands)) {
      return primaryRanks[0];
    }
    return primaryRanks[1];
  };
  