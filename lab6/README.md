# Lab 6 - Gameboard

In `lab6`, we will be creating a Gameboard featuring 2 widgets - `Day of the Week` and a `Poker Hand Calculator`.

#### Expectations

Overall, we will learn / re-enforce the following concepts:

1. Bringing in `JavaScript` files to a `ReactJS` project
2. Adding form fields to a `ReactJS` page using Grid
3. React Router
4. Separating ReactJS Components into Separate Files

### Part 0: Start with the Base

To begin, work from the `lab6-base` project in this directory.

To install the packages, simply run `npm install` from the root directory of the project `lab6-base/`

Once you perform the installation of the required libraries, you can now run the app using `npm start`

### Part 1: React Router

The first part of this lab is to create a React Router using the `react-router-dom` library. This will allow you to create different pages. In this lab, we will be creating 3 pages.

1. A Home Page that allows you to get to 2 sub pages.
2. A Day of the Week Page that contains code from the previous `lab5`
3. A 5 Poker Game that runs through a game of poker between 2 plays and displays the winner using code written in `lab6`

You'll want to start by added `react-router-dom` to your `lab6` project. Here's the documentation

https://reactrouter.com/en/main/start/tutorial

Once installed, you'll want to start creating new files

Until now, we've been working with `App.jsx`. Now, we will be moving away from `App.jsx` with 3 new components that reflect the 3 pages we are creating

To start, you'll want to setup a `routes` directory within the `src` directory. Within this, we will have the following files

- `src/routes/root.jsx`
- `src/routes/dow-page.jsx`
- `src/routes/poker-page.jsx`

Each of these files will act as a component that will render one of the pages

The first main objective is to make the `root.jsx` page have `2 buttons`. One button that will navigate to the `Day of the Week` page and another one that will navigate to the `Poker` page.

Below is an example of what the root component might look like

```
export default function Root() {

return (
    <>
      <div className="App">
        <header className="App-header">
          <h1>Your Content</h1>
        </header>
      </div>
    </>
)
}
```

Keep in mind, this example is using the original `App` and `App-header` class names which use `css` properties from the `App.css` file. You can continue to use this or you can create a new style sheet

Within this component, create 2 `MUI` buttons that link to both the `Poker` and `Day of the Week` pages

Note: You'll need to look up how to navigate to different pages

Next, we will setup React Router within the `index.js` file. Add a `router` below and replace the `<App />` with the `router`

```
const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
  }
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
```

Next, lets add a few routes. Look up how to add new routes to a router, and add a `Poker` page and a `Day of the Week` page.

Once you have the links, create new components where you display a `<h1>THIS IS THE <BLANK> PAGE </h1>` on each respective page

When you have the buttons working and you are routed to the pages, you've completed this step.

Signature `________________________________`

### Part 2: Day of the Week

For the next challenge, you'll be porting the code you wrote over from the last `lab5` to the Day of the Week component you made. This should be a simple port. You might need to make modifications to ensure that your app is styled correctly

Once your `lab5` code is ported over, call us over to show us the working Day of the Week page

Signature `________________________________`

### Part 3: 5 Card Poker Game

Once you have React Router working, it's time to build the poker game!

The Poker Game is described below. Your task will be to use the existing Poker code and create a `ReactJS` application to interface with the game. This is the majority of the exercise.

#### Poker Game Instructions

In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in the following way:

**High Card**: Highest <u>value</u> card.<br>
**One Pair**: Two cards of the same <u>value</u>.<br>
**Two Pairs**: Two different pairs.<br>
**Three of a Kind**: Three cards of the same <u>value</u>.<br>
**Straight**: All cards are consecutive <u>value</u>s.<br>
**Flush**: All cards of the same <u>suit</u>.<br>
**Full House**: Three of a kind and a pair.<br>
**Four of a Kind**: Four cards of the same <u>value</u>.<br>
**Straight Flush**: All cards are consecutive <u>value</u>s of same <u>suit</u>.<br>
**Royal Flush**: Ten, Jack, Queen, King, Ace, in same <u>suit</u>.<br>

The cards are valued in the order:
2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace

If two players have the same ranked hands then the rank made up of the highest value wins; for example, a pair of eights beats a pair of fives (see example 1 below). But if two ranks tie, for example, both players have a pair of queens, then highest cards in each hand are compared (see example 4 below); if the highest cards tie then the next highest cards are compared, and so on

Consider the following five hands dealt to two players:

| Hand | Player 1       |                                   | Player 2       |                                    | Winner   |
| ---- | -------------- | --------------------------------- | -------------- | ---------------------------------- | -------- |
| 1    | 5H 5C 6S 7S KD | Pair of Fives                     | 2C 3S 8S 8D TD | Pair of Eights                     | Player 2 |
| 2    | 5D 8C 9S JS AC | Highest card Ace                  | 2C 5C 7D 8S QH | Highest card Queen                 | Player 1 |
| 3    | 2D 9C AS AH AC | Three Aces                        | 3D 6D 7D TD QD | Flush with Diamonds                | Player 2 |
| 4    | 4D 6S 9H QH QC | Pair of Queens, Highest card Nine | 3D 6D 7H QD QS | Pair of Queens, Highest card Seven | Player 1 |
| 5    | 2H 2D 4C 4D 4S | Full House With Three Fours       | 3C 3D 3S 9S 9D | Full House with Three Threes       | Player 1 |

The file, poker.json, contains one-thousand random hands dealt to two players. Each line of the file contains ten cards (each card is separated by a single space): the first five are Player 1's cards and the last five are Player 2's cards. You can assume that all hands are valid (no invalid characters or repeated cards), each player's hand is in no specific order, and in each hand there is a clear winner.

We have setup a sample script that shows you how many hands each player has won from the Poker.json file.

To run this, you can simply execute the following

`cd src/poker; npm run solve`

You should see an output of how many hands have been won

`Player one wins XX hands
Player two wins XX hands`

This code will be the core use-case of the exercise, please read through it and try to understand it.

#### Interface Instructions

The game will allow you to walk through a game of 5 card poker where there are `5 game stages`:

1. `Start of Game`
2. `The Draw` - 3 Cards are pulled from a deck of cards per player
3. `The Turn` - 1 additional card is pulled from the deck of cards per player (each player has 4 cards total)
4. `The River` - 1 additional and the last card is pulled from the deck of cards per player (each play will have 5 cards total)
5. `End of Game` - The game results should be evaluated using the poker hand evaluation code you wrote for `lab3`

This game is meant to be for **2 players** not more

You must have some button that indicates to start the game, iterate through the turns, and refresh the game once a game is completed

At the end of the game, you should display the winner and the combination that won the player the hand

Finally, during the entire game, each plays cards that are drawn should be visible.

#### Lets get to it

Be creative in this exercise and get started!

Here is an example of an implementation

##### Dashboard

![Dashboard](./images/dashboard.png)

##### Day of the Week

![Dashboard](./images/day_of_the_week.png)

##### Poker Game Start

![Dashboard](./images/poker_start.png)

##### The Flop

![Dashboard](./images/poker_flop.png)

##### The Turn

![Dashboard](./images/poker_turn.png)

##### The River

![Dashboard](./images/poker_river.png)

##### End of the Poker Game - Results

![Dashboard](./images/poker_end.png)

Signature `________________________________`

### Part 4: Pushing to Gitlab

Once you completed all parts of the lab, and you have a verified working solution, you're ready to commit your progress to Git just like you did in previous labs.

Let us know when you've pushed your code to GitLab.

Signature `________________________________`

#### Congrats! You've completed Lab 6!
